package com.hlub.dev.demoemployeemvp.utils;

import java.text.DecimalFormat;

public class FormatCurrence {
    public static String formatVnCurrence(double price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        String str1 = formatter.format(Math.floor(price));

        return String.valueOf(str1);
    }
}
