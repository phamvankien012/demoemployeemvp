package com.hlub.dev.demoemployeemvp.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;
import com.hlub.dev.demoemployeemvp.update_employee.UpdateEmployeeActivity;
import com.hlub.dev.demoemployeemvp.utils.DialogHelper;
import com.hlub.dev.demoemployeemvp.utils.FormatCurrence;

import java.io.Serializable;

public class EmployeeViewHolder1 extends RecyclerView.ViewHolder implements IClickItem {

    private TextView tvItemName;
    private TextView tvItemAge;
    private TextView tvItemMoney;
    private ImageView imgItemDelete;
    private LinearLayout containerUpdate;

    private Context context;

    public EmployeeViewHolder1(View itemView, Context context) {
        super(itemView);
        this.context = context;
        initView();
    }

    private void initView() {
        tvItemName = itemView.findViewById(R.id.tvItemName);
        tvItemAge = itemView.findViewById(R.id.tvItemAge);
        tvItemMoney = itemView.findViewById(R.id.tvItemMoney);
        imgItemDelete = itemView.findViewById(R.id.imgItemDelete);
        containerUpdate = itemView.findViewById(R.id.container_update);
    }

    public void bind(final Employee employee, final int pos) {

        tvItemName.setText(employee.getEmployeeName());
        tvItemMoney.setText(FormatCurrence.formatVnCurrence(Double.parseDouble(employee.getEmployeeSalary())) + " VNĐ");
        tvItemAge.setText(employee.getEmployeeAge());

        containerUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickedToUpdate(employee);
            }
        });

        imgItemDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDeleteItemEmployee(Integer.parseInt(employee.getId()), pos);
            }
        });
    }

    @Override
    public void onItemClickedToUpdate(Employee employee) {
        context.startActivity(new Intent(context, UpdateEmployeeActivity.class));
        Intent intent=new Intent(context, UpdateEmployeeActivity.class);
        intent.putExtra("employee", employee);
    }

    @Override
    public void onClickDeleteItemEmployee(int id, int pos) {
        DialogHelper.customDialogDelete(context, id, pos);
    }
}
