package com.hlub.dev.demoemployeemvp.update_employee.presenter;

import android.content.Context;

import com.google.gson.JsonObject;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;
import com.hlub.dev.demoemployeemvp.update_employee.model.UpdateEmployeeInteractor;
import com.hlub.dev.demoemployeemvp.update_employee.view.IUpdateEmployeeView;
import com.hlub.dev.demoemployeemvp.utils.ConnectionHelper;

import okhttp3.ResponseBody;

public class UpdateEmployeePresenter {
    private IUpdateEmployeeView iUpdateEmployeeView;
    private Context context;

    private UpdateEmployeeInteractor updateEmployeeInteractor = new UpdateEmployeeInteractor();

    public UpdateEmployeePresenter(IUpdateEmployeeView iUpdateEmployeeView, Context context) {
        this.iUpdateEmployeeView = iUpdateEmployeeView;
        this.context = context;
    }

    public void updateEmployee(int id, String name,String age,String salary) {

        if(!ConnectionHelper.isNetworkAvailable(context)){
            iUpdateEmployeeView.onValidateUpdateEmployee("Không có kết nối mạng");
            return;
        }
        if(name.isEmpty()||age.isEmpty()||salary.isEmpty()){
            iUpdateEmployeeView.onValidateUpdateEmployee("Không được để dữ liệu rỗng");
            return;
        }

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("name",name);
        jsonObject.addProperty("age",age);
        jsonObject.addProperty("salary",salary);

        updateEmployeeInteractor.updateEmployee(id, jsonObject, new IRetrofitCallback<ResponseBody>() {
            @Override
            public void onSucess(ResponseBody obj) {
                iUpdateEmployeeView.onUpdateEmployeeSuccess();
            }

            @Override
            public void onError(String error) {
                iUpdateEmployeeView.onUpdateEmployeeError("Cập nhật nhân viên không thành công");
            }
        });
    }

}
