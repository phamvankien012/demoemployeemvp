package com.hlub.dev.demoemployeemvp.update_employee.model;

import com.google.gson.JsonObject;
import com.hlub.dev.demoemployeemvp.retrofit.EmployeeRetrofit;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateEmployeeInteractor {

    public void updateEmployee(int id, JsonObject jsonObject, final IRetrofitCallback<ResponseBody> listener) {
        EmployeeRetrofit.getInstance().updateEmployee(id, jsonObject).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSucess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onError("Có lỗi xảy ra");
            }
        });
    }

}
