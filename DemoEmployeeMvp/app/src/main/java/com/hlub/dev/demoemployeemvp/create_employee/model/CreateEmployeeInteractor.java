package com.hlub.dev.demoemployeemvp.create_employee.model;

import android.util.Log;

import com.google.gson.JsonObject;
import com.hlub.dev.demoemployeemvp.retrofit.EmployeeRetrofit;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEmployeeInteractor {

    public void createEmployee(JsonObject jsonObject, final IRetrofitCallback<ResponseBody> listener) {
        EmployeeRetrofit.getInstance().createEmployee(jsonObject).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSucess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onError("Có lỗi xảy ra. Vui lòng thử lại");
            }
        });
    }
}
