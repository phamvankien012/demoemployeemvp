package com.hlub.dev.demoemployeemvp.create_employee.presenter;

import android.content.Context;

import com.google.gson.JsonObject;
import com.hlub.dev.demoemployeemvp.create_employee.model.CreateEmployeeInteractor;
import com.hlub.dev.demoemployeemvp.create_employee.view.ICreateEmployeeView;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;
import com.hlub.dev.demoemployeemvp.utils.ConnectionHelper;

import okhttp3.ResponseBody;

public class CreateEmployeePresenter {

    private ICreateEmployeeView iCreateEmployeeView;
    private Context context;

    private CreateEmployeeInteractor createEmployeeInteractor = new CreateEmployeeInteractor();

    public CreateEmployeePresenter(Context context, ICreateEmployeeView iCreateEmployeeView) {
        this.iCreateEmployeeView = iCreateEmployeeView;
        this.context = context;
    }

    public void createEmployee(String name, String age, String salary) {
        if (name.isEmpty()) {
            iCreateEmployeeView.onValidateError("Tên trống");
            return;
        }
        if (age.isEmpty()) {
            iCreateEmployeeView.onValidateError("Tuổi trống");
            return;
        }
        if (salary.isEmpty()) {
            iCreateEmployeeView.onValidateError("Lương trống");
            return;
        }
        if (!ConnectionHelper.isNetworkAvailable(context)) {
            iCreateEmployeeView.onCreateEmployeeError("Không có kết nối mạng");
            return;
        }
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("age", age);
        jsonObject.addProperty("salary", salary);

        createEmployeeInteractor.createEmployee(jsonObject, new IRetrofitCallback<ResponseBody>() {
            @Override
            public void onSucess(ResponseBody obj) {
                iCreateEmployeeView.onCreateEmployeeSuccess(jsonObject);
                iCreateEmployeeView.setEmpityView();
            }

            @Override
            public void onError(String error) {
                iCreateEmployeeView.onCreateEmployeeError("Tạo nhân viên không thành công");
            }
        });
    }
}
