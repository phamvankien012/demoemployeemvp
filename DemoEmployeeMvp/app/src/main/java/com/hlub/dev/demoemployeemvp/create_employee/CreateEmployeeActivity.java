package com.hlub.dev.demoemployeemvp.create_employee;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.google.gson.JsonObject;
import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.create_employee.presenter.CreateEmployeePresenter;
import com.hlub.dev.demoemployeemvp.create_employee.view.ICreateEmployeeView;

public class CreateEmployeeActivity extends AppCompatActivity implements View.OnClickListener, ICreateEmployeeView {
    private Toolbar toolbarCreate;
    private ImageView imgBack;
    private EditText edtName;
    private EditText edtAge;
    private CurrencyEditText edtSalary;
    private Button btnCreate;

    private CreateEmployeePresenter createEmployeePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_employee);

        initView();

        btnCreate.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        createEmployeePresenter = new CreateEmployeePresenter(CreateEmployeeActivity.this, this);

    }

    private void initView() {

        toolbarCreate = findViewById(R.id.toolbar_create);
        imgBack = findViewById(R.id.imgBack);
        edtName = findViewById(R.id.edtName);
        edtAge = findViewById(R.id.edtAge);
        edtSalary = findViewById(R.id.edtSalary);
        btnCreate = findViewById(R.id.btnCreate);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.btnCreate) {

            String name = edtName.getText().toString();
            String age = edtAge.getText().toString();
            String salary = edtSalary.getText().toString();

            createEmployeePresenter.createEmployee(name, age, salary);

        }
        if (id == R.id.imgBack) {
            onBackPressed();
        }

    }

    @Override
    public void onCreateEmployeeSuccess(JsonObject jsonObject) {
        Toast.makeText(this, "Tạo nhân viên thành công", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateEmployeeError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidateError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setEmpityView() {
        edtName.setText("");
        edtAge.setText("");
        edtSalary.setText("");
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        finish();
        super.onBackPressed();
    }
}
