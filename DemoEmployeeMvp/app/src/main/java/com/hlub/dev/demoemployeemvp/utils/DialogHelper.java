package com.hlub.dev.demoemployeemvp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.list_employee.presenter.ListEmployeePresenter;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;

public class DialogHelper {

    public static ListEmployeePresenter listEmployeePresenter;
    private IListEmployeeView iListEmployeeView;

    public DialogHelper(ListEmployeePresenter listEmployeePresenter, IListEmployeeView iListEmployeeView) {
        this.listEmployeePresenter = listEmployeePresenter;
        this.iListEmployeeView = iListEmployeeView;
    }

    public static void customDialogDelete(Context context, final int id, final int pos) {
        final Dialog dialog = new Dialog(context);

        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_delete, null);

        TextView tvDeleteHuy = view.findViewById(R.id.tvDeleteHuy);
        TextView tvDeleteOK = view.findViewById(R.id.tvDeleteOK);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvDeleteHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvDeleteOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listEmployeePresenter.deleteItemEmployee(id, pos);
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }
}
