package com.hlub.dev.demoemployeemvp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.list_employee.presenter.ListEmployeePresenter;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    private Context context;
    private List<Employee> employeeList;
    private IListEmployeeView iListEmployeeView;
    private ListEmployeePresenter listEmployeePresenter;

    private final int ROW_1 = 1;
    private final int ROW_2 = 2;
    private final int ROW_3 = 3;

    public EmployeeAdapter(Context context, List<Employee> employeeList, IListEmployeeView iListEmployeeView) {
        this.context = context;
        this.employeeList = employeeList;
        this.iListEmployeeView = iListEmployeeView;
        listEmployeePresenter = new ListEmployeePresenter(context,iListEmployeeView);
    }

    @Override
    public int getItemViewType(int position) {
        int pos = position + 1;

        if (pos % 5 == 0) {
            return ROW_1;
        } else if (pos % 3 == 0) {
            return ROW_2;
        } else if (pos % 2 == 0) {
            return ROW_3;
        } else {
            return ROW_1;
        }

    }

    @NonNull
    @Override
    public EmployeeAdapter.EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ROW_1:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_a, parent, false));
            case ROW_2:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_b, parent, false));
            case ROW_3:
                return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_employee_c, parent, false));
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, final int position) {
        Random rand = new Random();
        //Lương nhân ramdom từ 50 -> 200 sau đó format thành 1.000.000 vnđ
        final double salary = Double.parseDouble(employeeList.get(position).getEmployeeSalary()) * (rand.nextInt(151) + 50);
        final String name = employeeList.get(position).getEmployeeName().toString();
        final String age = employeeList.get(position).getEmployeeAge().toString();
        final int id = Integer.parseInt(employeeList.get(position).getId().toString());

        EmployeeViewHolder employeeViewHolder = (EmployeeViewHolder) holder;
        employeeViewHolder.tvItemName.setText(name);
        employeeViewHolder.tvItemMoney.setText(formatVnCurrence(salary) + " VNĐ");
        employeeViewHolder.tvItemAge.setText(age);

        holder.containerUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iListEmployeeView.onItemClickedToUpdate();
            }
        });

        holder.imgItemDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iListEmployeeView.onClickDeleteItemEmployee(id, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return employeeList == null ? 0 : employeeList.size();
    }

    public class EmployeeViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemName;
        private TextView tvItemAge;
        private TextView tvItemMoney;
        private ImageView imgItemDelete;
        private LinearLayout containerUpdate;

        public EmployeeViewHolder(View itemView) {
            super(itemView);

            tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
            tvItemAge = (TextView) itemView.findViewById(R.id.tvItemAge);
            tvItemMoney = (TextView) itemView.findViewById(R.id.tvItemMoney);
            imgItemDelete = (ImageView) itemView.findViewById(R.id.imgItemDelete);
            containerUpdate = (LinearLayout) itemView.findViewById(R.id.container_update);

        }
    }


    public static String formatVnCurrence(double price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        String str1 = formatter.format(Math.floor(price));

        return String.valueOf(str1);
    }
}
