package com.hlub.dev.demoemployeemvp.adapter.holder;
import com.hlub.dev.demoemployeemvp.entity.Employee;


public interface IClickItem {
    void onItemClickedToUpdate(Employee employee);

    void onClickDeleteItemEmployee(int id, int pos);
}
