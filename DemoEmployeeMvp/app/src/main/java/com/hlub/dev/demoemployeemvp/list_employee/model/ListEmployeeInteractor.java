package com.hlub.dev.demoemployeemvp.list_employee.model;

import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.retrofit.EmployeeRetrofit;
import com.hlub.dev.demoemployeemvp.retrofit.IEmployeeService;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListEmployeeInteractor {

    public void getListEmployee(final IRetrofitCallback<List<Employee>> listener) {
        EmployeeRetrofit.getInstance().getListEmployee().enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                listener.onSucess(response.body());
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                listener.onError("Có lỗi xảy ra. Vui lòng thử lại");
            }
        });
    }

    public void deleteEmployee(int id, final IRetrofitCallback<Employee> listener) {
        EmployeeRetrofit.getInstance().deleteEmployee(id).enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                listener.onSucess(response.body());
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                listener.onError("Có lỗi xảy ra. Vui lòng thử lại");
            }
        });
    }
}
