package com.hlub.dev.demoemployeemvp.utils;

public interface DialogCallback {
    void disAgree();

    void onAgree();
}
