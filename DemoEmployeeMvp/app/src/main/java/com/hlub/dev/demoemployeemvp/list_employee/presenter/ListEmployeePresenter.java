package com.hlub.dev.demoemployeemvp.list_employee.presenter;

import android.content.Context;

import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.list_employee.model.ListEmployeeInteractor;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;
import com.hlub.dev.demoemployeemvp.retrofit.IRetrofitCallback;
import com.hlub.dev.demoemployeemvp.utils.ConnectionHelper;

import java.util.List;


public class ListEmployeePresenter {
    private IListEmployeeView iListEmployeeView;
    private Context context;

    private ListEmployeeInteractor listEmployeeInteractor = new ListEmployeeInteractor();

    public ListEmployeePresenter(Context context, IListEmployeeView iListEmployeeView) {
        this.iListEmployeeView = iListEmployeeView;
        this.context = context;
    }

    public void getListEmployee() {
        if (ConnectionHelper.isNetworkAvailable(context)) {
            listEmployeeInteractor.getListEmployee(new IRetrofitCallback<List<Employee>>() {
                @Override
                public void onSucess(List<Employee> obj) {
                    iListEmployeeView.onGetListEmployeeSuccess(obj);
                }

                @Override
                public void onError(String error) {
                    iListEmployeeView.onGetListEmployeeError("Lấy danh sách nhân viên không thành công");

                }
            });
        } else {
            iListEmployeeView.onGetListEmployeeError("Không có kết nối mạng");
        }
    }

    public void deleteItemEmployee(int id, final int pos) {
        listEmployeeInteractor.deleteEmployee(id, new IRetrofitCallback<Employee>() {
            @Override
            public void onSucess(Employee obj) {
                iListEmployeeView.onDeleteEmployeeSuccess(pos);
            }

            @Override
            public void onError(String error) {
                iListEmployeeView.onDeleteEmployeeError(error);
            }
        });
    }
}
