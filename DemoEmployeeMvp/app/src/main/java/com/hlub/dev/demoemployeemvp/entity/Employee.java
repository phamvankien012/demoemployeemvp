package com.hlub.dev.demoemployeemvp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Employee implements Serializable {
    @Expose
    private String id;
    @Expose
    private String employee_name;
    @Expose
    private String employee_salary;
    @Expose
    private String employee_age;
    @Expose
    private String profile_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employee_name;
    }

    public void setEmployeeName(String employeeName) {
        this.employee_name = employeeName;
    }

    public String getEmployeeSalary() {
        return employee_salary;
    }

    public void setEmployeeSalary(String employeeSalary) {
        this.employee_salary = employeeSalary;
    }

    public String getEmployeeAge() {
        return employee_age;
    }

    public void setEmployeeAge(String employeeAge) {
        this.employee_age = employeeAge;
    }

    public String getProfileImage() {
        return profile_image;
    }

    public void setProfileImage(String profileImage) {
        this.profile_image = profileImage;
    }
}
