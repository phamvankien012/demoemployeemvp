package com.hlub.dev.demoemployeemvp.create_employee.view;

import com.google.gson.JsonObject;

public interface ICreateEmployeeView {

    void onCreateEmployeeSuccess(JsonObject jsonObject);

    void onCreateEmployeeError(String error);

    void onValidateError(String error);

    void setEmpityView();
}
