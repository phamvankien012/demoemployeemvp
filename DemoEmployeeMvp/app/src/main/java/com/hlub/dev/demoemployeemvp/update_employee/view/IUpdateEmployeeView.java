package com.hlub.dev.demoemployeemvp.update_employee.view;

public interface IUpdateEmployeeView {
    void onUpdateEmployeeSuccess();
    void onUpdateEmployeeError(String error);
    void onValidateUpdateEmployee(String error);

}
