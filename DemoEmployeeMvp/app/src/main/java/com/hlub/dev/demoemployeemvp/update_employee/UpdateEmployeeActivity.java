package com.hlub.dev.demoemployeemvp.update_employee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;
import android.app.Activity;

import com.blackcat.currencyedittext.CurrencyEditText;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.list_employee.ListEmployeeActivity;
import com.hlub.dev.demoemployeemvp.update_employee.view.IUpdateEmployeeView;
import com.hlub.dev.demoemployeemvp.update_employee.presenter.UpdateEmployeePresenter;
import com.hlub.dev.demoemployeemvp.entity.Employee;

public class UpdateEmployeeActivity extends AppCompatActivity implements IUpdateEmployeeView, View.OnClickListener {
    private Toolbar toolbarUpdate;
    private ImageView imgBack;
    private EditText edtName;
    private EditText edtAge;
    private CurrencyEditText edtSalary;
    private Button btnUpdate;

    private UpdateEmployeePresenter updateEmployeePresenter;
    private Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_employee);

        initView();
        initClickListener();

        getDataIntent();
        setEditTextEmployee();
        updateEmployeePresenter = new UpdateEmployeePresenter(this, this);

    }

    private void setEditTextEmployee() {
        edtName.setText(employee.getEmployeeName());
        edtAge.setText(employee.getEmployeeAge());
        edtSalary.setText(employee.getEmployeeSalary());
    }

    private void getDataIntent() {
        Intent intent = new Intent();
        employee = (Employee) intent.getSerializableExtra("employee");
    }

    private void initClickListener() {
        btnUpdate.setOnClickListener(this);
    }

    private void initView() {
        toolbarUpdate = findViewById(R.id.toolbar_update);
        imgBack = findViewById(R.id.imgBack);
        edtName = findViewById(R.id.edtName);
        edtAge = findViewById(R.id.edtAge);
        edtSalary = findViewById(R.id.edtSalary);
        btnUpdate = findViewById(R.id.btnUpdate);
    }

    @Override
    public void onUpdateEmployeeSuccess() {
        Toast.makeText(this, "Cập nhật dữ liệu thành công", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(UpdateEmployeeActivity.this, ListEmployeeActivity.class));
    }

    @Override
    public void onUpdateEmployeeError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidateUpdateEmployee(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        updateEmployeePresenter.updateEmployee(Integer.parseInt(employee.getId()),
                edtName.getText().toString(),
                edtAge.getText().toString(),
                edtSalary.getText().toString());
    }
}
