package com.hlub.dev.demoemployeemvp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.adapter.holder.EmployeeViewHolder1;
import com.hlub.dev.demoemployeemvp.adapter.holder.EmployeeViewHolder2;
import com.hlub.dev.demoemployeemvp.adapter.holder.EmployeeViewHolder3;
import com.hlub.dev.demoemployeemvp.adapter.holder.NetworkViewHolder;
import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.list_employee.presenter.ListEmployeePresenter;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;
import com.hlub.dev.demoemployeemvp.utils.ConnectionHelper;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Employee> employeeList;
    private IListEmployeeView iListEmployeeView;
    private ListEmployeePresenter listEmployeePresenter;

    private final int ROW_1 = 1;
    private final int ROW_2 = 2;
    private final int ROW_3 = 3;
    private final int ROW_NETWORK = 0;

    private boolean isNetwork;

    public Adapter(Context context, List<Employee> employeeList, IListEmployeeView iListEmployeeView, ListEmployeePresenter listEmployeePresenter) {
        this.context = context;
        this.employeeList = employeeList;
        this.iListEmployeeView = iListEmployeeView;
        this.listEmployeePresenter = listEmployeePresenter;
        isNetwork = ConnectionHelper.isNetworkAvailable(context);
    }

    @Override
    public int getItemViewType(int position) {
        int pos = position + 1;

        if (!isNetwork) {
            return ROW_NETWORK;
        } else {
            if (pos % 5 == 0) {
                return ROW_1;
            } else if (pos % 3 == 0) {
                return ROW_2;
            } else if (pos % 2 == 0) {
                return ROW_3;
            } else {
                return ROW_1;
            }
        }

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            switch (viewType) {
                case ROW_1:
                    return new EmployeeViewHolder1(LayoutInflater.from(context).inflate(R.layout.item_employee_a, parent, false), context);
                case ROW_2:
                    return new EmployeeViewHolder2(LayoutInflater.from(context).inflate(R.layout.item_employee_b, parent, false),context);
                case ROW_3:
                    return new EmployeeViewHolder3(LayoutInflater.from(context).inflate(R.layout.item_employee_b, parent, false),context);
                case ROW_NETWORK:
                    return new NetworkViewHolder(LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false));
                default:
                    return null;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(! employeeList.isEmpty()){
            Employee employee = employeeList.get(position);

            //holder1
            EmployeeViewHolder1 holder1 = (EmployeeViewHolder1) holder;
            holder1.bind(employee,position);


            //holder2
            EmployeeViewHolder2 holder2 = (EmployeeViewHolder2) holder;
            holder2.bind(employee,position);

            //holder3
            EmployeeViewHolder3 holde3 = (EmployeeViewHolder3) holder;
            holde3.bind(employee,position);
        }
    }

    @Override
    public int getItemCount() {
        if (!isNetwork && employeeList.isEmpty()) {
            return 1;
        } else {
            return employeeList.size();
        }
    }
}
