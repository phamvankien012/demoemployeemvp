package com.hlub.dev.demoemployeemvp.retrofit;

public interface IRetrofitCallback<T> {
    void onSucess(T obj);

    void onError(String error);
}
