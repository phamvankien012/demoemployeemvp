package com.hlub.dev.demoemployeemvp.list_employee.view;

import com.hlub.dev.demoemployeemvp.entity.Employee;

import java.util.ArrayList;
import java.util.List;

public interface IListEmployeeView {

    void onGetListEmployeeSuccess(List<Employee> list);

    void onGetListEmployeeError(String error);

    void onDeleteEmployeeSuccess(int pos);

    void onDeleteEmployeeError(String error);

    void onItemClickedToUpdate();

    void onClickDeleteItemEmployee(int id, int position);
}
