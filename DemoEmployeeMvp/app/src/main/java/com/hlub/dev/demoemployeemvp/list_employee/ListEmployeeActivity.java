package com.hlub.dev.demoemployeemvp.list_employee;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.hlub.dev.demoemployeemvp.R;
import com.hlub.dev.demoemployeemvp.adapter.Adapter;
import com.hlub.dev.demoemployeemvp.adapter.EmployeeAdapter;
import com.hlub.dev.demoemployeemvp.create_employee.CreateEmployeeActivity;
import com.hlub.dev.demoemployeemvp.entity.Employee;
import com.hlub.dev.demoemployeemvp.list_employee.presenter.ListEmployeePresenter;
import com.hlub.dev.demoemployeemvp.list_employee.view.IListEmployeeView;
import com.hlub.dev.demoemployeemvp.update_employee.UpdateEmployeeActivity;
import com.hlub.dev.demoemployeemvp.utils.DialogHelper;

import java.util.ArrayList;
import java.util.List;

public class ListEmployeeActivity extends AppCompatActivity implements IListEmployeeView, View.OnClickListener,SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recycleviewEmployee;
    private FloatingActionButton flAddEmployee;

    private List<Employee> employeeList;

    private ListEmployeePresenter listEmployeePresenter;
    private Adapter employeeAdapter;
    private DialogHelper dialogHelper;
    private SwipeRefreshLayout swipeContainer;

    private static int RES_CREATE = 1;
    private static int RES_UPDATE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_employee);

        initView();
        initRecycleView();
        initSwipeRefesh();

        listEmployeePresenter = new ListEmployeePresenter(ListEmployeeActivity.this, this);
        listEmployeePresenter.getListEmployee();

        flAddEmployee.setOnClickListener(this);
        dialogHelper = new DialogHelper(listEmployeePresenter, this);

    }

    private void initSwipeRefesh() {
        swipeContainer.setOnRefreshListener(this);

        swipeContainer.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

    }

    private void initRecycleView() {
        employeeList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycleviewEmployee.setLayoutManager(linearLayoutManager);
        employeeAdapter = new Adapter(this, employeeList, this, listEmployeePresenter);
        recycleviewEmployee.setAdapter(employeeAdapter);
    }

    private void initView() {
        swipeContainer = findViewById(R.id.swipe_container);
        recycleviewEmployee = findViewById(R.id.recycleviewEmployee);
        flAddEmployee = findViewById(R.id.flAddEmployee);
    }

    @Override
    public void onGetListEmployeeSuccess(List<Employee> list) {
        employeeList.addAll(list);
        employeeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGetListEmployeeError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flAddEmployee:
                Intent intent = new Intent(ListEmployeeActivity.this, CreateEmployeeActivity.class);
                startActivityForResult(intent, RES_CREATE);

        }
    }

    @Override
    public void onItemClickedToUpdate() {
        startActivity(new Intent(ListEmployeeActivity.this, UpdateEmployeeActivity.class));
    }

    @Override
    public void onClickDeleteItemEmployee(int id, int pos) {
        dialogHelper.customDialogDelete(this, id, pos);
    }

    @Override
    public void onDeleteEmployeeSuccess(int pos) {
        employeeList.remove(pos);
        employeeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeleteEmployeeError(String error) {
        Toast.makeText(this, "Xóa nhân viên không thành công", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRefresh() {
        onGetListEmployeeSuccess(employeeList);
    }
}
