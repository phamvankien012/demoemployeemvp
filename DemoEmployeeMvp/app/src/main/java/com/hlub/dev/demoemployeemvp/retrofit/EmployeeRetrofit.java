package com.hlub.dev.demoemployeemvp.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmployeeRetrofit {

    public static IEmployeeService iEmployeeService;

    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static IEmployeeService getInstance() {
        if (iEmployeeService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://dummy.restapiexample.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            iEmployeeService = retrofit.create(IEmployeeService.class);
        }
        return iEmployeeService;
    }
}
